<?php

namespace Drupal\message_expire;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Expires messages from the system based on global and template configurations.
 */
class MessageExpiryManager implements MessageExpiryManagerInterface {

  /**
   * The entity type manager service.
   *
   * @var EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs the purging service.
   *
   * @param EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * @inheritDoc
   */
  public function expire(array $messages) {
    foreach ($messages as $message) {
      $message->set(MessageExpiryManagerInterface::MESSAGE_EXPIRE_FIELD, TRUE);
      $message->save();
    }
  }
}

