<?php

namespace Drupal\message_expire\Plugin\MessagePurge;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Queue\QueueInterface;
use Drupal\message\MessagePurgeBase;
use Drupal\message\MessageTemplateInterface;
use Drupal\message\Plugin\MessagePurge\Days;
use Drupal\message_expire\MessageExpiryManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Delete messages older than certain days.
 *
 * @MessagePurge(
 *   id = "expire_after",
 *   label = @Translation("Expire after", context = "MessagePurge"),
 *   description = @Translation("Expire messages older than a given amount of days."),
 * )
 */
class ExpireAfter extends Days {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('message')->getQuery(),
      $container->get('queue')->get('message_expire'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritDoc}
   */
  protected function baseQuery(MessageTemplateInterface $template) {
    $qry = parent::baseQuery($template);

    // Exclude already expired messages.
    $qry->condition(MessageExpiryManagerInterface::MESSAGE_EXPIRE_FIELD, 0, '!=');

    return $qry;
  }

  /**
   * {@inheritdoc}
   */
  public function fetch(MessageTemplateInterface $template) {
    $mids = parent::fetch($template);

    // Allow expiring messages to be altered.
    $mids = \Drupal::moduleHandler()->invokeAll('message_expire_alter', $mids);

    return $mids;
  }
}
