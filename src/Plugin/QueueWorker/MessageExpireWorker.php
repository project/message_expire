<?php

namespace Drupal\message_expire\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\message_expire\MessageExpiryManagerInterface;
use Drupal\message\MessageInterface;

/**
 * Deletes a set of messages.
 *
 * No more than MessagePurgeInterface::MESSAGE_DELETE_SIZE messages should be
 * given to a single queue item to ensure that the worker can complete the task
 * within PHP operating constraints.
 *
 * @QueueWorker(
 *   id = "message_expire",
 *   title = @Translation("Expire messages"),
 *   cron = {"time" = 10}
 * )
 */
class MessageExpireWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The message storage handler.
   *
   * @var MessageExpiryManagerInterface
   */
  protected MessageExpiryManagerInterface $messageExpiryManager;

  /**
   * The message storage handler.
   *
   * @var ContentEntityStorageInterface
   */
  protected ContentEntityStorageInterface $messageStorage;

  /**
   * Constructs a new MessageDeletionWorker object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param MessageExpiryManagerInterface $message_expiry_manager
   *   The message expiry manager.
   * @param EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, MessageExpiryManagerInterface $message_expiry_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->messageExpiryManager = $message_expiry_manager;
    $this->messageStorage = $entity_type_manager->getStorage('message');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('message_expire.manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (!empty($data)) {
      /** @var MessageInterface[] $messages */
      $messages = $this->messageStorage->loadMultiple($data);
      $this->messageExpiryManager->expire($messages);
    }
  }

}
