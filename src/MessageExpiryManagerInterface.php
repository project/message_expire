<?php

namespace Drupal\message_expire;

use Drupal\message\MessageInterface;

/**
 * Expires messages from the system based on global and template configurations.
 */
interface MessageExpiryManagerInterface {

  const MESSAGE_EXPIRE_FIELD = 'expire';

  /**
   * Expire messages.
   *
   * @param MessageInterface[] $messages
   *   An array of messages to expire.
   */
  public function expire(array $messages);
}

